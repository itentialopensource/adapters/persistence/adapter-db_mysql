# MySQL

Vendor: Oracle
Homepage: https://www.oracle.com/

Product: MySQL
Product Page: https://www.mysql.com/

## Introduction
We classify MySQL into the Data Storage domaina as MySQL is a database which provides the storage of information. 

"MySQL powers the most demanding Web, E-commerce, SaaS and Online Transaction Processing (OLTP) applications"

## Why Integrate
The MySQL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Oracle MySQL. With this adapter you have the ability to perform operations with MySQL on items such as:

- Storage of Information
- Retrieval of Information

## Additional Product Documentation
The [MySQL SQL Reference](https://dev.mysql.com/doc/refman/8.4/en/sql-statements.html)
The [MySQL Node Library Documentation](https://www.npmjs.com/package/mysql)