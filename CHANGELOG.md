
## 0.2.4 [10-15-2024]

* Changes made at 2024.10.14_21:22PM

See merge request itentialopensource/adapters/adapter-db_mysql!21

---

## 0.2.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-db_mysql!19

---

## 0.2.2 [08-07-2024]

* Changes made at 2024.08.07_10:51AM

See merge request itentialopensource/adapters/adapter-db_mysql!18

---

## 0.2.1 [07-26-2024]

* manual migration updates

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!16

---

## 0.2.0 [01-06-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!12

---

## 0.1.16 [06-30-2023]

* Remove sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!11

---

## 0.1.15 [08-03-2022]

* Patch/adapt 2277

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!9

---

## 0.1.14 [08-01-2022]

* Patch/adapt 2277

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!8

---

## 0.1.13 [08-01-2022]

* Changed connection handling. Added healthcheck.

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!7

---

## 0.1.12 [08-10-2021]

* change the connection and base wait

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!6

---

## 0.1.11 [08-10-2021]

* add delays in the connect retries

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!5

---

## 0.1.10 [08-06-2021]

* include Raj changes for connection recovery as well as other stuff

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!4

---

## 0.1.9 [11-26-2019]

* Patch/add ssl

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!3

---

## 0.1.8 [11-26-2019]

* Patch/add ssl

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!3

---

## 0.1.7 [11-25-2019]

* improve README.

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!1

---

## 0.1.6 [11-21-2019]

* Bug fixes and performance improvements

See commit 469d9ab

---

## 0.1.5 [11-21-2019]

* Bug fixes and performance improvements

See commit 6b15e9a

---

## 0.1.4 [11-21-2019]

* Bug fixes and performance improvements

See commit a6d2f9a

---
