## Troubleshoot

### Connectivity Issues

1. Verify the adapter properties are set up correctly.

```text
Go into the Itential Platform GUI and verify/update the properties
```

2. Verify there is connectivity between the Itential Platform Server and MicrosoftSQL Server.

```text
ping the ip address of MicrosoftSQL server
try telnet to the ip address port of MicrosoftSQL
execute a curl command to the other system
```

3. Verify the credentials provided for MicrosoftSQL.

```text
login to MicrosoftSQL using the provided credentials
```


### Functional Issues

Adapter logs are located in `/var/log/pronghorn`. In older releases of the Itential Platform, there is a `pronghorn.log` file which contains logs for all of the Itential Platform. In newer versions, adapters can be configured to log into their own files.
