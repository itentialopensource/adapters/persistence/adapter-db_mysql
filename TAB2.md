# MySQL

## Table of Contents 

  - [Specific Adapter Information](#specific-adapter-information) 
    - [Authentication](#authentication) 
    - [Sample Properties](#sample-properties) 
    - [Swagger](#swagger) 
  - [Generic Adapter Information](#generic-adapter-information) 

## Specific Adapter Information
### Authentication

This document will go through the steps for authenticating the MySQL adapter with. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

#### Database Library Authentication
The MySQL adapter requires Database Library Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a MySQL server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "dbAuth": true,
  "username": "username",
  "password": "Password"
}
```
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

#### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Investigate the logs
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
### Sample Properties

Sample Properties can be used to help you configure the adapter in the Itential Automation Platform. You will need to update connectivity information such as the host, port, protocol and credentials.

```json
  "properties": {
    "database": "your database name",
    "host": "localhost",
    "port": 3306,
    "authentication": {
      "dbAuth": false,
      "username": "username",
      "password": "password"
    },
    "ssl": {
      "enabled": false,
      "accept_invalid_cert": false,
      "ca_file": ""
    },
    "connectionOptions": {
      "connectTimeout": 10000,
      "acquireTimeout": 10000,
      "waitForConnections": true,
      "connectionLimit": 10,
      "queueLimit": 0
    },
    "healthcheck": {
      "type": "intermittent",
      "frequency": 60000
    }
  }
```
### Swagger

Note: The content for this section may be missing as its corresponding .json file is unavailable. This sections will be updated once adapter-openapi.json file is added.
## [Generic Adapter Information](https://gitlab.com/itentialopensource/adapters/adapter-db_mysql/-/blob/master/README.md) 

