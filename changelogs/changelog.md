
## 0.1.16 [06-30-2023]

* Remove sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!11

---

## 0.1.15 [08-03-2022]

* Patch/adapt 2277

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!9

---

## 0.1.14 [08-01-2022]

* Patch/adapt 2277

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!8

---

## 0.1.13 [08-01-2022]

* Changed connection handling. Added healthcheck.

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!7

---

## 0.1.12 [08-10-2021]

* change the connection and base wait

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!6

---

## 0.1.11 [08-10-2021]

* add delays in the connect retries

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!5

---

## 0.1.10 [08-06-2021]

* include Raj changes for connection recovery as well as other stuff

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!4

---

## 0.1.9 [11-26-2019]

* Patch/add ssl

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!3

---

## 0.1.8 [11-26-2019]

* Patch/add ssl

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!3

---

## 0.1.7 [11-25-2019]

* improve README.

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!1

---

## 0.1.6 [11-21-2019]

* Bug fixes and performance improvements

See commit 469d9ab

---

## 0.1.5 [11-21-2019]

* Bug fixes and performance improvements

See commit 6b15e9a

---

## 0.1.4 [11-21-2019]

* Bug fixes and performance improvements

See commit a6d2f9a

---
